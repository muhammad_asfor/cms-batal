import 'package:dartz/dartz.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetPageDetails extends UseCase<PageModel, GetPageDetailsParams> {
  final SettingsRepository repository;

  GetPageDetails(
    this.repository,
  );

  @override
  Future<Either<Failure, PageModel>> call(GetPageDetailsParams params) async {
    return await repository.getPageDetails(
      id: params.id,
    );
  }
  
}

class GetPageDetailsParams {
  final int id;
  GetPageDetailsParams({
    required this.id,
  });
}
