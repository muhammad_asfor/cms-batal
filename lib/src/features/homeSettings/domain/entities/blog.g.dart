// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Blog _$BlogFromJson(Map<String, dynamic> json) => Blog(
      title: json['title'] as String,
      description: json['description'] as String,
      coverImage: json['cover_image'] as String,
      id: json['id'] as int,
      imagesBag: (json['images_bag'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$BlogToJson(Blog instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'cover_image': instance.coverImage,
      'description': instance.description,
      'images_bag': instance.imagesBag,
    };
