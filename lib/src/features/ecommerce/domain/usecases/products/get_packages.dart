import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/MyPackages.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class Getpackages extends UseCase<MyPackages, NoParams> {
  final EcommerceRepository repository;
  Getpackages(this.repository);

  @override
  Future<Either<Failure, MyPackages>> call(NoParams params) async {
    return await repository.getMyPackages();
  }
}
