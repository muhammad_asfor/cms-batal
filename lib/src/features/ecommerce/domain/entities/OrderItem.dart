import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/OrderProduct.dart';
part 'OrderItem.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class OrderItem {
  final int id;
  final String? uuid;
  final String? paymentMethod;
  final String? orderMethod;
  final String totalText;
  final String? shippingStatusText;
  final List<String>? itemsThumbnails;
  final String? status;
  final String? createdAt;
  final List<OrderProduct>? itemsList;
  final bool? isCancellable;
  final String? feesText;
  final String? subtotaltext;
  final String? trackingUrl;
  /**status
   * 'pending'
'accepted'
'processing'
'refused'
'canceled' 
'delivered'
'returned'
   */
  OrderItem(
      {required this.id,
      this.itemsList,
      this.trackingUrl,
      this.isCancellable,
      this.itemsThumbnails,
      this.orderMethod,
      this.paymentMethod,
      this.shippingStatusText,
      required this.totalText,
      this.uuid,
      this.feesText,
      this.subtotaltext,
      this.createdAt,
      this.status});

  factory OrderItem.fromJson(json) => _$OrderItemFromJson(json);
  toJson() => _$OrderItemToJson(this);

  static List<OrderItem> fromJsonList(List json) {
    return json.map((e) => OrderItem.fromJson(e)).toList();
  }
}
