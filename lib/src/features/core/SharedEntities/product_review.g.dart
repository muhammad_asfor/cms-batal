// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductReview _$ProductReviewFromJson(Map<String, dynamic> json) =>
    ProductReview(
      json['id'] as int?,
      json['name'] as String?,
      json['cover_image'] as String?,
      json['rate'] as int?,
      json['comment'] as String?,
      json['created_at'] as String?,
    );

Map<String, dynamic> _$ProductReviewToJson(ProductReview instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'cover_image': instance.coverImage,
      'rate': instance.rate,
      'comment': instance.comment,
      'created_at': instance.createdAt,
    };
