import 'package:json_annotation/json_annotation.dart';
import 'Token.dart';
part 'User.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class User {
  String? name;
  final String? email;
  final String? website;
  final String? address;
  final int? id;
  final String? slug;
  final String? profilePhotoUrl;
  final String? coverImage;
  final int? locationId;
  final String? refLink;

  final String? mobile;
  final String? countryCode;
  @JsonKey(fromJson: _statusFromJson)
  final int? status;
  final int? city_id;
  final int? state_id;
  final int? grade;

  String? password;

  final String? createdAt;
  final String? userType;
  final bool? hasPendingRequest;

  final String? emailVerifiedAt;
  final Token? oauth;

  /// For Batal
  final String? countryText;
  final String? stateText;
  final bool? isTechnician;
  User({
    this.status,
    this.emailVerifiedAt,
    this.createdAt,
    this.city_id,
    this.grade,
    this.hasPendingRequest,
    this.state_id,
    this.userType,
    this.oauth,
    this.refLink,
    this.name,
    required this.email,
    this.countryCode,
    this.countryText,
    this.stateText,
    this.mobile,
    this.website,
    this.address,
    required this.id,
    this.slug,
    this.profilePhotoUrl,
    this.coverImage,
    this.password,
    this.locationId,
    this.isTechnician,
  });
  factory User.fromJson(json) => _$UserFromJson(json);
  toJson() => _$UserToJson(this);

  static int? _statusFromJson(json) {
    if (json == null) return null;
    if (json is bool) {
      return json ? 1 : 0;
    } else if (json is int) {
      return json;
    }
    return json;
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is User && o.id == id;
  }

  @override
  int get hashCode {
    return id.hashCode;
  }
}
