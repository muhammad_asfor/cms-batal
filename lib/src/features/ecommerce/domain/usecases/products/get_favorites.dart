import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/FavoriteItem.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetFavorites extends UseCase<List<FavoriteItem>, GetFavoritesParams> {
  final EcommerceRepository repository;
  GetFavorites(this.repository);

  @override
  Future<Either<Failure, List<FavoriteItem>>> call(
      GetFavoritesParams params) async {
    return await repository.getFavorites(page: params.page);
  }
}

class GetFavoritesParams {
  final int page;

  GetFavoritesParams({
    required this.page,
  });
}
