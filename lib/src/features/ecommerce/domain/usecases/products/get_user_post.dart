import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/UserPost.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetUserPost extends UseCase<UserPost, NoParams> {
  final EcommerceRepository repository;
  GetUserPost(this.repository);

  @override
  Future<Either<Failure, UserPost>> call(NoParams params) async {
    return await repository.getUserPost();
  }
}
