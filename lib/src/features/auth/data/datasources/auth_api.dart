import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/auth/data/datasources/AuthSharedPref.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/get_public_token.dart';
import 'package:progiom_cms/src/features/auth/domain/entities/UplodeImage.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/login.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/refreash_token.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Token.dart';

import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';
import 'package:progiom_cms/src/features/core/network/network.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../homeSettings.dart';
import '../../settings.dart';
import 'dart:developer' as dev;

class AuthApi {
  AuthApi();

  static final Dio tokenDio = Dio(BaseOptions(
    responseType: ResponseType.json,
    baseUrl: sl<Dio>().options.baseUrl,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "charset": "utf-8",
      "Accept-Charset": "utf-8",
      "Accept-Language":
          sl<SharedPreferences>().getString(AuthConstants.Language) ?? "ar",
      "currency":
          sl<SharedPreferences>().getString(AuthConstants.Currency) ?? "USD",
      "country-id": sl<SharedPreferences>().getString("country") ?? "",
      "state-id": sl<SharedPreferences>().getString("state") ?? "",
      "city-id": sl<SharedPreferences>().getString("city") ?? "",
    },
  ))
    ..interceptors.add(LoggerInterceptor(
      logger,
      request: true,
      requestBody: true,
      error: true,
      responseBody: true,
      responseHeader: false,
      requestHeader: true,
    ));

  static addTokenToHeader(String accessToken, bool isPublicToken) {
    sl<Dio>()
        .interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options, r) {
          options.headers["Authorization"] = "Bearer " + accessToken.toString();

          return r.next(options);
        }, onError: (error, handler) async {
          try {
            if ((error.response?.statusCode != null) &&
                (error.response?.statusCode) == 401) {
              //**
              // 1- in home settings api || login api if returned 401
              // then public token is expired and need to call initPublicToken
              // 2-if any athor api returned 401 then you are logged in and need to logged in again
              //if public token used and returned 401 then we will generate new one
              //but if logged token returnd 401 then will generate the two token.
              // */
              sl<Dio>().lock();
              sl<Dio>().interceptors.responseLock.lock();
              sl<Dio>().interceptors.errorLock.lock();
              // dev.debugger();
              if (isPublicToken) {
                await GetPublicToken(sl()).call(NoParams());
                await continueAndRepeatRequest(error, handler);
              } else {
                tokenDio.interceptors.add(
                    InterceptorsWrapper(onError: (DioError error, r) async {
                  if ((error.response!.statusCode) == 401) {
                    // public token expired and logged in
                    onUnexpectedError(handler, error);
                  } else {
                    r.reject(error);
                  }
                }));

                final refreashToken =
                    await sl<AuthSharedPrefs>().getRefreashToken();
                await RefreashToken(sl()).call(RefreashTokenParams(
                  refreashToken: refreashToken,
                ));

                await continueAndRepeatRequest(error, handler);
              }
            } else
              return handler.next(error);
          } catch (e) {
            onUnexpectedError(handler, error);
          }
        }));
  }

  // static addFilter(String accessToken, bool isPublicToken) {
  //   sl<Dio>().interceptors.add(InterceptorsWrapper(
  //       onRequest: (RequestOptions options, r) {
  //         options.headers["Authorization"] = "Bearer " + accessToken.toString();

  //         return r.next(options);
  //       },
  //       onError: (error, handler) async {}));
  // }

  static Future<void> onUnexpectedError(
      ErrorInterceptorHandler handler, error) async {
    await GetPublicToken(sl()).call(NoParams());
    sl<Dio>().unlock();
    sl<Dio>().interceptors.responseLock.unlock();
    sl<Dio>().interceptors.errorLock.unlock();
    sl<Dio>().clear();
    handler.reject(error);
    sl<HomesettingsBloc>().add(GetSettings());

    AuthSettings.navigateLoginCall();
  }

  static Future<void> continueAndRepeatRequest(
      DioError error, ErrorInterceptorHandler handler) async {
    sl<Dio>().unlock();
    sl<Dio>().interceptors.responseLock.unlock();
    sl<Dio>().interceptors.errorLock.unlock();
    print("reshed in continue");
    return await sl<Dio>().fetch(error.response!.requestOptions).then(
      (r) {
        handler.resolve(r);
      },
      onError: (e) {
        return handler.reject(e);
      },
    );
  }

  Future<User> login(
      {required String email, required String password, Dio? tkDio}) async {
    try {
      final formData = {
        (email.contains(new RegExp(r'[A-Za-z]')) ? "email" : "mobile"): email,
        "password": password,
        "client_id": AuthSettings.clientPasswordId,
        "client_secret": AuthSettings.clientPasswordSecret,
        "grant_type": "password"
      };

      var response =
          await (tkDio ?? sl<Dio>()).post('/api/login', data: formData);

      final User user = User.fromJson(response.data['data']);
      clearDioInterceptors();
      addTokenToHeader(user.oauth!.accessToken!, false);
      return user;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<User> socialLogin({
    required String token,
    required String provider,
  }) async {
    var secrets =
        "client_id=${AuthSettings.clientPasswordId}&client_secret=${AuthSettings.clientPasswordSecret}&grant_type=social";
    try {
      var response = await sl<Dio>().get(
        '/api/login/$provider$token&$secrets',
      );

      final User user = User.fromJson(response.data['data']);
      clearDioInterceptors();
      addTokenToHeader(user.oauth!.accessToken!, false);
      return user;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> requestChangePassword({
    required String email,
  }) async {
    try {
      await sl<Dio>().post('/api/password', data: {"email": email});

      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> validateResetCode(
      {required String email, required String code}) async {
    try {
      await sl<Dio>()
          .post('/api/password-check', data: {"email": email, "token": code});

      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> changePassword({
    required String email,
    required String code,
    required String password,
  }) async {
    try {
      await sl<Dio>().post('/api/password-update', data: {
        "email": email,
        "token": code,
        "password": password,
        "password_confirmation": password
      });

      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<void> logout() async {
    try {
      await sl<Dio>().get(
        '/api/logout',
      );
      clearDioInterceptors();
      return;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  static clearDioInterceptors() {
    sl<Dio>().interceptors.clear();
    sl<Dio>().interceptors.add(LoggerInterceptor(
          logger,
          request: true,
          requestBody: true,
          error: true,
          responseBody: true,
          responseHeader: false,
          requestHeader: true,
        ));
  }

  Future<User> register({
    required Map<String, dynamic> user,
  }) async {
    try {
      final formData = {
        "email": user["email"],
        "name": user["name"] ?? null,
        "password": user["password"],
        "password_confirmation": user["password"],
        "country_code": user["country_code"] ?? null,
        "mobile": user["mobile"] ?? null,
        "website": user["website"] ?? null,
        "address": user["address"] ?? null,
        "image": user["coverImage"] ?? null,
        "client_id": AuthSettings.clientPasswordId,
        "client_secret": AuthSettings.clientPasswordSecret,
        "grant_type": "password"
      };

      var response = await sl<Dio>().post('/api/register', data: formData);

      final User newUser = User.fromJson(response.data['data']);
      clearDioInterceptors();
      addTokenToHeader(newUser.oauth!.accessToken!, false);
      return newUser;
    } on DioError catch (e) {
      if (e.response?.statusCode == 422 && e.response?.data['errors'] != null) {
        throw ServerException((e.response?.data['errors'] as Map)
                .values
                .first
                ?.first
                .toString() ??
            (handleDioError(e)));
      } else
        throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<Token> getPublicToken() async {
    try {
      final formData = {
        "client_id": AuthSettings.clientId,
        "client_secret": AuthSettings.clientSecret,
        "grant_type": "client_credentials",
        "scope": "*"
      };

      var response = await tokenDio.post('/oauth/token', data: formData);

      final Token token = Token.fromJson(response.data);
      if (token.accessToken == null) {
        throw ServerException("token is null");
      } else {
        clearDioInterceptors();
        addTokenToHeader(token.accessToken!, true);
      }

      return token;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  static Future<Token> refreashToken(String refreashToken) async {
    try {
      print("refreashing");
      final formData = {
        "client_id": AuthSettings.clientPasswordId,
        "client_secret": AuthSettings.clientPasswordSecret,
        "grant_type": "refresh_token",
        "refresh_token": refreashToken
      };

      var response = await tokenDio.post('/oauth/token', data: formData);

      final Token token = Token.fromJson(response.data);
      if (token.accessToken == null) {
        throw ServerException("token null");
      } else {
        clearDioInterceptors();
        addTokenToHeader(token.accessToken!, false);
      }

      return token;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<User> getMyProfile() async {
    try {
      var response = await sl<Dio>().get('/api/user',
          options: Options(headers: {
            "country-id": sl<SharedPreferences>().getString("country") ?? "",
            "state-id": sl<SharedPreferences>().getString("state") ?? "",
            "city-id": sl<SharedPreferences>().getString("city") ?? "",
          }));

      return User.fromJson(response.data);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  setUploadImage(File file) async {
    String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "media[]": await MultipartFile.fromFile(file.path, filename: fileName),
    });
    var response = await sl<Dio>().post('/api/upload', data: formData);
    return uplodeImage.fromJson(response.data);
  }

  Future<void> updateProfile(Map<String, dynamic> data) async {
    try {
      if (data["bool"]) {
        var listImage = await setUploadImage(data["image[]"]);
        FormData formData = FormData.fromMap({
          "name": data["name"],
          "country_code": data["country_code"],
          "mobile": data["mobile"],
          "image[]": listImage.url,
          "country_id": data["country_id"],
          "state_id": data["state_id"],
          "city_id": data["city_id"],
        });

        await sl<Dio>().post('/api/user', data: formData);
      } else {
        await sl<Dio>().post('/api/user', data: data);
      }

      return;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<void> setFcmToken(String token) async {
    try {
      await sl<Dio>().post('/api/user', data: {"fcm_token_app": token});

      return;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
