import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/core.dart';
import 'dart:developer' as dev;

part 'FavoriteItem.g.dart';
@JsonSerializable(fieldRename: FieldRename.snake)
class FavoriteItem {
  final int id;
  final int userId;
  final int itemId;
  final String title;
  final Product item;

  FavoriteItem(
      {required this.id,
      required this.userId,
      required this.itemId,
      required this.title,
      required this.item});

  factory FavoriteItem.fromJson(json) => _$FavoriteItemFromJson(json);
  toJson() => _$FavoriteItemToJson(this);

  static List<FavoriteItem> fromJsonList(List json) {
    return json.map((e) => FavoriteItem.fromJson(e)).toList();
  }
}
