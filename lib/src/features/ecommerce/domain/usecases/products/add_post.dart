import 'dart:io';

import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class AddPost extends UseCase<bool, AddPostParams> {
  final EcommerceRepository repository;

  AddPost(this.repository);

  @override
  Future<Either<Failure, bool>> call(AddPostParams params) async {
    return await repository.addPost(
        parentId: params.parentId,
        title: params.title,
        description: params.description,
        images: params.images,
        imagesold: params.imagesold!,
        id: params.id);
  }
}

class AddPostParams {
  final int parentId;
  final String title;
  final String description;
  final List<File> images;
  List<String>? imagesold;
  String? id;

  AddPostParams({
    required this.parentId,
    required this.title,
    required this.description,
    required this.images,
    this.imagesold,
    this.id,
  });
}
