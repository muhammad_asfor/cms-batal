// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'User.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      status: User._statusFromJson(json['status']),
      emailVerifiedAt: json['email_verified_at'] as String?,
      createdAt: json['created_at'] as String?,
      city_id: json['city_id'] as int?,
      grade: json['grade'] as int?,
      hasPendingRequest: json['has_pending_request'] as bool?,
      state_id: json['state_id'] as int?,
      userType: json['user_type'] as String?,
      oauth: json['oauth'] == null ? null : Token.fromJson(json['oauth']),
      refLink: json['ref_link'] as String?,
      name: json['name'] as String?,
      email: json['email'] as String?,
      countryCode: json['country_code'] as String?,
      countryText: json['country_text'] as String?,
      stateText: json['state_text'] as String?,
      mobile: json['mobile'] as String?,
      website: json['website'] as String?,
      address: json['address'] as String?,
      id: json['id'] as int?,
      slug: json['slug'] as String?,
      profilePhotoUrl: json['profile_photo_url'] as String?,
      coverImage: json['cover_image'] as String?,
      password: json['password'] as String?,
      locationId: json['location_id'] as int?,
      isTechnician: json['is_technician'] as bool?,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
      'website': instance.website,
      'address': instance.address,
      'id': instance.id,
      'slug': instance.slug,
      'profile_photo_url': instance.profilePhotoUrl,
      'cover_image': instance.coverImage,
      'location_id': instance.locationId,
      'ref_link': instance.refLink,
      'mobile': instance.mobile,
      'country_code': instance.countryCode,
      'status': instance.status,
      'city_id': instance.city_id,
      'state_id': instance.state_id,
      'grade': instance.grade,
      'password': instance.password,
      'created_at': instance.createdAt,
      'user_type': instance.userType,
      'has_pending_request': instance.hasPendingRequest,
      'email_verified_at': instance.emailVerifiedAt,
      'oauth': instance.oauth,
      'country_text': instance.countryText,
      'state_text': instance.stateText,
      'is_technician': instance.isTechnician,
    };
