import 'package:json_annotation/json_annotation.dart';

part 'CheckoutModel.g.dart';

enum PaymentMethods { cod, wire_transfer }

@JsonSerializable(fieldRename: FieldRename.snake)
class CheckoutModel {
  final String notes;
  final int addressId;
  final PaymentMethods paymentMethod;
  CheckoutModel(
      {required this.addressId, required this.paymentMethod, this.notes = ""});

  factory CheckoutModel.fromJson(json) => _$CheckoutModelFromJson(json);
  toJson() => _$CheckoutModelToJson(this);

  static List<CheckoutModel> fromJsonList(List json) {
    return json.map((e) => CheckoutModel.fromJson(e)).toList();
  }
}
