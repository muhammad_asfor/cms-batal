class LogginChecker {
  final bool isLoggedIn;
  final bool isFirstLaunch;
  final String? token;

  LogginChecker(
      {required this.isLoggedIn, required this.isFirstLaunch, this.token});
}
