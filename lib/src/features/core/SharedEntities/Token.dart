import 'package:json_annotation/json_annotation.dart';

part 'Token.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Token {
  final String? tokenType;
  final int? expiresIn;
  final String? accessToken;
  final String? refreshToken;

  Token({this.accessToken, this.expiresIn, this.tokenType, this.refreshToken});
  factory Token.fromJson(json) => _$TokenFromJson(json);
  toJson() => _$TokenToJson(this);
}
