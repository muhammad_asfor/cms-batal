// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'option_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OptionData _$OptionDataFromJson(Map<String, dynamic> json) => OptionData(
      title: json['title'] as String?,
      optionId: json['option_id'] as int,
      selectedOption: json['selected_option'] as String?,
      allowedOptions: json['allowed_options'] as Map<String, dynamic>,
    );

Map<String, dynamic> _$OptionDataToJson(OptionData instance) =>
    <String, dynamic>{
      'option_id': instance.optionId,
      'allowed_options': instance.allowedOptions,
      'title': instance.title,
      'selected_option': instance.selectedOption,
    };
