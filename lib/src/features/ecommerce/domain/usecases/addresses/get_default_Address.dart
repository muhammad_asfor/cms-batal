import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/entities.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetDefaultAddress extends UseCase<Address, NoParams> {
  final EcommerceRepository repository;
  GetDefaultAddress(this.repository);

  @override
  Future<Either<Failure, Address>> call(NoParams noParams) async {
    return await repository.getDefaultAddress();
  }
}
