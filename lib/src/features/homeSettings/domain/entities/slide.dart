import 'package:json_annotation/json_annotation.dart';
part 'slide.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Slide {
  final int id;
  final String entity;
  final int? entityId;
  final String? link;
  final List<String>? images;
  final int status;
  final String? slug;
  final int? sorting;
  final String createdAt;
  final String? coverImage;
  final String? title;
  factory Slide.fromJson(json) => _$SlideFromJson(json);
  toJson() => _$SlideToJson(this);
  static List<Slide> fromJsonList(List json) {
    return json.map((e) => Slide.fromJson(e)).toList();
  }

  Slide(
      {required this.id,
      required this.createdAt,
      required this.status,
      required this.entity,
      this.coverImage,
      this.entityId,
      this.images,
      this.link,
      this.slug,
      this.sorting,
      this.title});
}
