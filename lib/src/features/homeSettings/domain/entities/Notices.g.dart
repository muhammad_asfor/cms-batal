// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Notices.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Notices _$NoticesFromJson(Map<String, dynamic> json) => Notices(
      type: json['type'] as String?,
      title: json['title'] as String?,
      text: json['text'] as String?,
    );

Map<String, dynamic> _$NoticesToJson(Notices instance) => <String, dynamic>{
      'type': instance.type,
      'title': instance.title,
      'text': instance.text,
    };
