import 'package:package_info_plus/package_info_plus.dart';

String getIosVersionNumber(PackageInfo value) => value.version
    .replaceRange(value.version.length - 3, value.version.length - 1, "");
