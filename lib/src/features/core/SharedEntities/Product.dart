import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/comment.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/field_data.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/filter_data.dart';
part 'Product.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Product {
  final int id;
  final String? createdAt;
  final String? coverImage;
  bool? isFavorite;
  final String? priceText;
  final String title;
  final List<String>? images;
  final int? status;
  final String? slug;
  final int? sorting;
  final int? views;
  final price;
  final currencyId;
  final String? presalePriceText;
  var rating;
  final String? updatedAt;
  final List<String>? imagesBag;
  final String? publicLink;
  final String? description;
  final String? shortDescription;
  final User? user;
  final int? pointsPrice;
  final List<Product>? relatedItems;
  final List<OptionData>? optionsData;
  final List<ProductReview>? reviewsList;
  final List<FieldData>? fieldsData;
  final String? videoUrl;
  final List<String>? imagesData;

  /// For Batal
  final String? categoryText;
  final List<Comment>? comments;
  final int? featured;

  Product({
    this.images,
    this.status,
    this.pointsPrice,
    this.publicLink,
    this.rating,
    this.shortDescription,
    this.slug,
    this.reviewsList,
    this.presalePriceText,
    this.imagesData,
    this.relatedItems,
    this.optionsData,
    this.sorting,
    this.views,
    this.price,
    this.currencyId,
    this.updatedAt,
    this.imagesBag,
    this.description,
    this.user,
    this.fieldsData,
    this.videoUrl,
    this.categoryText,
    this.comments,
    this.featured,
    required this.id,
    required this.createdAt,
    required this.coverImage,
    required this.isFavorite,
    required this.priceText,
    required this.title,
  });

  factory Product.fromJson(json) => _$ProductFromJson(json);
  toJson() => _$ProductToJson(this);

  static List<Product> fromJsonList(List json) {
    return json.map((e) => Product.fromJson(e)).toList();
  }
}
