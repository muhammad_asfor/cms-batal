part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class LoginEvent extends AuthEvent {
  final String email;
  final String password;
  LoginEvent({required this.email, required this.password});
}

class LoginSocialEvent extends AuthEvent {
  final SocialLoginProvider provider;
  final String token;
  LoginSocialEvent({required this.provider, required this.token});
}

class SignUpEvent extends AuthEvent {
  final Map<String, dynamic> user;
  SignUpEvent(this.user);
}

class LogoutEvent extends AuthEvent {}

class InitToken extends AuthEvent {
  final bool isAfterLogout;
  InitToken({required this.isAfterLogout});
}

class GetMyProfileEvent extends AuthEvent {}

class UpdateProfileEvent extends AuthEvent {
  final Map<String, dynamic> data;
  UpdateProfileEvent(this.data);
}
