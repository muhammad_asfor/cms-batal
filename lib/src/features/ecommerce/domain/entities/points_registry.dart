import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/core.dart';

part 'points_registry.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class PointsRegistry {
  final User user;

  final String? date;
  final String? points;
  final String? type;
  PointsRegistry({required this.user, this.type, this.points, this.date});

  factory PointsRegistry.fromJson(json) => _$PointsRegistryFromJson(json);
  toJson() => _$PointsRegistryToJson(this);

  static List<PointsRegistry> fromJsonList(List json) {
    return json.map((e) => PointsRegistry.fromJson(e)).toList();
  }
}
