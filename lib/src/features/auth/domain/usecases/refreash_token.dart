import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';

class RefreashToken extends UseCase<String, RefreashTokenParams> {
  final AuthRepository repository;
  RefreashToken(this.repository);
  @override
  Future<Either<Failure, String>> call(RefreashTokenParams params) async {
    return await repository.refreashToken(
      refreashToken: params.refreashToken);
  }
}

class RefreashTokenParams {
  final String refreashToken;
  RefreashTokenParams({
     required this.refreashToken});
}
