import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:equatable/equatable.dart';
import 'package:dartz/dartz.dart';

class ValidateResetPassword extends UseCase<bool, ValidateResetPasswordParams> {
  final AuthRepository repository;
  ValidateResetPassword(this.repository);
  @override
  Future<Either<Failure, bool>> call(ValidateResetPasswordParams params) async {
    return await repository.validateResetCode(
        code: params.resetCoode, email: params.email);
  }
}

class ValidateResetPasswordParams {
  final String email;
  final String resetCoode;
  ValidateResetPasswordParams({required this.email, required this.resetCoode});
}
