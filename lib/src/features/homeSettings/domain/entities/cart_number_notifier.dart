import 'package:flutter/material.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/usecases/get_preferences.dart';

class CartNumber extends ChangeNotifier {
  int number = 0;
  // void add() {
  //   number++;
  //   // This call tells the widgets that are listening to this model to rebuild.
  //   notifyListeners();
  // }

  void remove() {
    number--;
    notifyListeners();
  }

  // void clear() {
  //   number = 0;
  //   notifyListeners();
  // }

  void sync() async {
    final resutl = await GetPreferences(sl()).call(NoParams());
    resutl.fold((l) {}, (r) {
      number = r["cart_items_count"] ?? 0;
      notifyListeners();
    });
  }

  void set(int initValue) async {
    // to not call it every time we call change
    if (number == 0) {
      number = initValue;
      notifyListeners();
    }
  }

  CartNumber._(this.number);

  static final CartNumber instance = CartNumber._(0);
}
