import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class SearchProducts extends UseCase<List<Product>, SearchProductsParams> {
  final EcommerceRepository repository;

  SearchProducts(this.repository);

  @override
  Future<Either<Failure, List<Product>>> call(
      SearchProductsParams params) async {
    String queryParams = "";
    if (params.minPrice != null || params.maxPrice != null) {
      queryParams += "&price[min]=${params.minPrice ?? 0}";
      queryParams += "&price[max]=${params.maxPrice ?? 1400}";
    }
    if (params.orderColumn != null) {
      queryParams += "&order[column]=${params.orderColumn}";
    }
    if (params.orderDirection != null) {
      queryParams += "&order[dir]=${params.orderDirection}";
    }
    if (params.rating != null) {
      queryParams += "&rating=${params.rating}";
    }
    if (params.categoryId != null) {
      queryParams += "&parent_id=${params.categoryId}";
    }
    if (params.countryId != null) {
      queryParams += "&country_id=${params.countryId}";
    }
    if (params.stateId != null) {
      queryParams += "&state_id=${params.stateId}";
    }
    if (params.cityId != null) {
      queryParams += "&city_id=${params.cityId}";
    }

    if (params.query.trim().isNotEmpty)
      repository.saveLocalQuery(query: params.query);
    return await repository.getListProducts(
        endpoint: "/posts" +
            "?page=" +
            params.page.toString() +
            (params.query.isEmpty ? "" : "&q=") +
            params.query +
            queryParams,
        filterValues: params.filterValues);
  }
}

class SearchProductsParams {
  final int page;
  final String query;
  final String? minPrice;
  final String? maxPrice;

  final String? orderColumn;
  final String? orderDirection;
  final int? categoryId;
  final int? rating;
  final Map<int, dynamic>? filterValues;

  /// For Batal
  final int? countryId;
  final int? stateId;
  final int? cityId;

  SearchProductsParams({
    required this.query,
    required this.page,
    this.minPrice,
    this.maxPrice,
    this.orderColumn,
    this.orderDirection,
    this.categoryId,
    this.rating,
    this.filterValues,
    this.countryId,
    this.stateId,
    this.cityId,
  });
}
