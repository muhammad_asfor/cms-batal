import 'package:dartz/dartz.dart';
import '../../../../../core.dart';
import '../../domain/repositories/rateApp_repository.dart';

import '../data_sources/rateApp_shared_prefs.dart';

class RateAppRepositoryImpl extends RateAppRepository {
  final RateAppSharedPrefs rateAppPrefs;
  RateAppRepositoryImpl(this.rateAppPrefs);

  @override
  Future<Either<Failure, String?>> initRateTime() async {
    try {
      return Right(await rateAppPrefs.initRateTime());
    } on CacheException catch (e) {
      return Left(CacheFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> register() async {
    try {
      return Right(await rateAppPrefs.register());
    } on CacheException catch (e) {
      return Left(CacheFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> disableRate() async {
    try {
      return Right(await rateAppPrefs.disableRate());
    } on CacheException catch (e) {
      return Left(CacheFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> rateLater() async {
    try {
      return Right(await rateAppPrefs.rateLater());
    } on CacheException catch (e) {
      return Left(CacheFailure(e.message));
    }
  }
}
