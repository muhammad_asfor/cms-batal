import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';

class UpdateProfile extends UseCase<void, UpdateProfileParams> {
  final AuthRepository repository;
  UpdateProfile(this.repository);

  @override
  Future<Either<Failure, void>> call(UpdateProfileParams params) async {
    return await repository.updateProfile(params.data);
  }
}

class UpdateProfileParams {
  final Map<String, dynamic> data;
  UpdateProfileParams(this.data);
}
