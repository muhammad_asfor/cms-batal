import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/homeSettings.dart';

class CheckoutPoints extends UseCase<bool, CheckoutPointsParams> {
  final EcommerceRepository repository;
  CheckoutPoints(this.repository);

  @override
  Future<Either<Failure, bool>> call(CheckoutPointsParams params) async {
    final result =
        await repository.checkoutPoints(params.addressId, params.itemId,params.selectedOptions);
    CartNumber.instance.sync();
    return result;
  }
}

class CheckoutPointsParams {
  final int addressId;
  final int itemId;
  final Map<String, String>? selectedOptions;
  CheckoutPointsParams(
      {required this.addressId, required this.itemId, this.selectedOptions});
}
