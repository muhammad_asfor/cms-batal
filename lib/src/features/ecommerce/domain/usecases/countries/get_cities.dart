import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/cityModel.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetCities extends UseCase<List<CityModel>, GetCitiesParams> {
  final EcommerceRepository repository;
  GetCities(this.repository);

  @override
  Future<Either<Failure, List<CityModel>>> call(GetCitiesParams params) async {
    return await repository.getCities(
        countryId: params.countryId, stateId: params.stateId);
  }
}

class GetCitiesParams {
  final int? countryId;
  final int? stateId;
  GetCitiesParams({this.countryId, this.stateId})
      : assert(countryId != null || stateId != null);
}
