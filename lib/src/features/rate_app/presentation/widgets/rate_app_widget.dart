import 'package:flutter/material.dart';

showRateDialoge(context, sizeConfig) {
  showDialog(
      context: context,
      builder: (context) => Center(
            child: SizedBox(
              height: sizeConfig.h(275),
              width: sizeConfig.h(350),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            "assets/logo.png",
                            height: sizeConfig.h(100),
                            width: sizeConfig.h(100),
                          ),
                        ],
                      ),
                      Spacer(flex: 1),
                      Text(
                        "rate app",
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        "rate app descrption",
                        style: TextStyle(fontSize: 16),
                      ),
                      Spacer(
                        flex: 2,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                "back",
                                style: TextStyle(fontSize: 14),
                              )),
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                                Navigator.pushNamed(context, "/login",
                                    arguments: true);
                              },
                              child: Text(
                                "rate",
                                style: TextStyle(fontSize: 14),
                              ))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ));
}
