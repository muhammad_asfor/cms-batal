// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UseCouponResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UseCouponResponse _$UseCouponResponseFromJson(Map<String, dynamic> json) =>
    UseCouponResponse(
      newPrice: json['new_price'] as String,
    );

Map<String, dynamic> _$UseCouponResponseToJson(UseCouponResponse instance) =>
    <String, dynamic>{
      'new_price': instance.newPrice,
    };
