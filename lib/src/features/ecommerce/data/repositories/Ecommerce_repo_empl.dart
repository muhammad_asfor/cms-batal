import 'dart:io';

import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/error/exceptions.dart';
import 'package:progiom_cms/src/features/ecommerce/data/datasources/EcommerceSharedPrefs.dart';
import 'package:progiom_cms/src/features/ecommerce/data/datasources/Ecommerce_api.dart';
import 'package:progiom_cms/src/features/core/util/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/Address.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CartModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CheckoutModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/MyPackages.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/UseCouponResponse.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/UserPost.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/cityModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CountryModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/FavoriteItem.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/OrderItem.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/my_points_response.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/points_registry.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/states_model.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class EcommerceRepositoryImpl extends EcommerceRepository {
  final EcommerceApi ecommerceApi;
  final EcommerceSharedPrefs ecommerceSharedPrefs;

  EcommerceRepositoryImpl(this.ecommerceApi, this.ecommerceSharedPrefs);

  final List<CountryModel> countries = [];
  Address? defaultAddress;

  @override
  Future<Either<Failure, List<Product>>> getListProducts(
      {required String endpoint, Map<int, dynamic>? filterValues}) async {
    try {
      final products = await ecommerceApi.getListProducts(
          endpoint: endpoint, filterValues: filterValues);

      return Right(products);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<FavoriteItem>>> getFavorites(
      {required int page}) async {
    try {
      final favorites = await ecommerceApi.getFavorites(page: page);

      return Right(favorites);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> setIsFavorites(
      {required bool isFavorite, required String itemId}) async {
    try {
      final result = await ecommerceApi.setIsFavorites(
          itemId: itemId, isFavorite: isFavorite);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<OrderItem>>> getOrders(
      {required int page}) async {
    try {
      final result = await ecommerceApi.getOrders(page: page);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, Product>> getProductDetails(
      {required String id}) async {
    try {
      final result = await ecommerceApi.getProductDetails(id: id);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, Address>> getDefaultAddress() async {
    if (defaultAddress != null) {
      return Future.value(Right(defaultAddress!));
    } else {
      await getMyAddresses();
      if (defaultAddress != null) {
        return Right(defaultAddress!);
      } else {
        // no addresses
        return Left(FakeFailure("No Addresses for this user! add one."));
      }
    }
  }

  @override
  Future<Either<Failure, List<Address>>> getMyAddresses() async {
    try {
      final result = await ecommerceApi.getListAddresses();
      if (result.isNotEmpty) {
        defaultAddress = result.first;
      } else {
        defaultAddress = null;
      }
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, String>> url2id(String url) async {
    try {
      final result =
          await ecommerceApi.url2id(url.substring(url.lastIndexOf("/")));

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> addPost({
    required int parentId,
    required String title,
    required String description,
    required List<File> images,
    List<String>? imagesold,
    String? id,
  }) async {
    try {
      final result = await ecommerceApi.addPost(
          parentId: parentId,
          title: title,
          description: description,
          images: images,
          imagesold: imagesold!,
          id: id ?? null);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> addAddress({
    required int cityId,
    required int countryId,
    int? stateId,
    required String name,
    required String description,
  }) async {
    try {
      final result = await ecommerceApi.addAddress(
          cityId: cityId,
          countryId: countryId,
          stateId: stateId,
          name: name,
          description: description);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> updateAddress({
    required int cityId,
    required int countryId,
    required int id,
    required String name,
    required String description,
  }) async {
    try {
      final result = await ecommerceApi.updateAddress(
          cityId: cityId,
          countryId: countryId,
          id: id,
          name: name,
          description: description);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> deleteAddress(int id) async {
    try {
      final result = await ecommerceApi.deleteAddress(id);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<void> saveLocalQuery({required String query}) async {
    await ecommerceSharedPrefs.saveLocalQuery(query);
  }

  @override
  List<String>? getLocalQueries() {
    return ecommerceSharedPrefs.getLocalQueries();
  }

  @override
  Future<Either<Failure, bool>> setDefaultAddress(
      {required int addressId}) async {
    try {
      final result = await ecommerceApi.setDefaultAddress(addressId);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, Address>> getAddressDetails(int id) async {
    try {
      final result = await ecommerceApi.getAddressDetails(id);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, OrderItem>> getOrderDetails(
      {required int orderId}) async {
    try {
      final result = await ecommerceApi.getOrderDetails(orderId);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> reviewOrderProduct(
      {required int itemId,
      required int rating,
      required String comment}) async {
    try {
      final result = await ecommerceApi.reviewOrderProduct(
          itemId: itemId, comment: comment, rating: rating);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> checkout(CheckoutModel checkoutModel) async {
    try {
      final result = await ecommerceApi.checkout(checkoutModel);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, CartModel>> getCart() async {
    try {
      final result = await ecommerceApi.getCart();

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> addToCart(
      {required int productId,
      required int qty,
      Map<String, String>? selectedOptions}) async {
    try {
      final result = await ecommerceApi.addToCart(
          productId: productId, qty: qty, selectedOptions: selectedOptions);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> updateCartItem({
    required int cartItemId,
    required int qty,
  }) async {
    try {
      final result =
          await ecommerceApi.updateCartItem(cartItemId: cartItemId, qty: qty);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, UseCouponResponse>> useCoupon({
    required String code,
  }) async {
    try {
      final result = await ecommerceApi.useCoupon(
        code: code,
      );

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> deleteFromCart({
    required int cartItemId,
  }) async {
    try {
      final result = await ecommerceApi.deleteFromCart(cartItemId: cartItemId);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<CountryModel>>> getCountries() async {
    try {
      if (countries.isNotEmpty) return Right(countries);
      final result = await ecommerceApi.getCountries();
      countries.clear();
      countries.addAll(result);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<CityModel>>> getCities(
      {int? countryId, int? stateId}) async {
    try {
      final result =
          await ecommerceApi.getCities(countryId: countryId, stateId: stateId);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  void deleteSearchHistoryItem(String query) {
    ecommerceSharedPrefs.deleteSearchHistoryItem(query);
  }

  @override
  Future<Either<Failure, bool>> cancelOrder({required int orderId}) async {
    try {
      final result = await ecommerceApi.cancelOrder(orderId);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, MyPointsResponse>> getMyPoints() async {
    try {
      final result = await ecommerceApi.getMyPoints();

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> checkoutPoints(
      int addressId, int itemId, Map? selectedOptions) async {
    try {
      final result =
          await ecommerceApi.checkoutPoints(addressId, itemId, selectedOptions);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<StatesModel>>> getStates(int countryId) async {
    try {
      final result = await ecommerceApi.getStates(countryId);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<PointsRegistry>>> getMyPointsRegistry() async {
    try {
      final result = await ecommerceApi.getMyPointsRegistry();

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<Product>>> getListProductsall() async {
    try {
      final products = await ecommerceApi.getListProductsall();

      return Right(products);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, MyPackages>> getMyPackages() async {
    try {
      final products = await ecommerceApi.getMyPackages();

      return Right(products);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, UserPost>> getUserPost() async {
    try {
      final products = await ecommerceApi.getUserPost();

      return Right(products);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
