// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'field_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FieldData _$FieldDataFromJson(Map<String, dynamic> json) => FieldData(
      json['id'] as int,
      json['title'] as String?,
      json['values'],
      json['type'] as String,
      json['value'] as String?,
    );

Map<String, dynamic> _$FieldDataToJson(FieldData instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'value': instance.value,
      'values': instance.values,
      'type': instance.type,
    };
