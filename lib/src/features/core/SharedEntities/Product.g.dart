// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) => Product(
      images:
          (json['images'] as List<dynamic>?)?.map((e) => e as String).toList(),
      status: json['status'] as int?,
      pointsPrice: json['points_price'] as int?,
      publicLink: json['public_link'] as String?,
      rating: json['rating'],
      shortDescription: json['short_description'] as String?,
      slug: json['slug'] as String?,
      reviewsList: (json['reviews_list'] as List<dynamic>?)
          ?.map((e) => ProductReview.fromJson(e))
          .toList(),
      presalePriceText: json['presale_price_text'] as String?,
      imagesData: (json['images_data'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      relatedItems: (json['related_items'] as List<dynamic>?)
          ?.map((e) => Product.fromJson(e))
          .toList(),
      optionsData: (json['options_data'] as List<dynamic>?)
          ?.map((e) => OptionData.fromJson(e))
          .toList(),
      sorting: json['sorting'] as int?,
      views: json['views'] as int?,
      price: json['price'],
      currencyId: json['currency_id'],
      updatedAt: json['updated_at'] as String?,
      imagesBag: (json['images_bag'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      description: json['description'] as String?,
      user: json['user'] == null ? null : User.fromJson(json['user']),
      fieldsData: (json['fields_data'] as List<dynamic>?)
          ?.map((e) => FieldData.fromJson(e))
          .toList(),
      videoUrl: json['video_url'] as String?,
      categoryText: json['category_text'] as String?,
      comments: (json['comments'] as List<dynamic>?)
          ?.map((e) => Comment.fromJson(e))
          .toList(),
      featured: json['featured'] as int?,
      id: json['id'] as int,
      createdAt: json['created_at'] as String?,
      coverImage: json['cover_image'] as String?,
      isFavorite: json['is_favorite'] as bool?,
      priceText: json['price_text'] as String?,
      title: json['title'] as String,
    );

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'created_at': instance.createdAt,
      'cover_image': instance.coverImage,
      'is_favorite': instance.isFavorite,
      'price_text': instance.priceText,
      'title': instance.title,
      'images': instance.images,
      'status': instance.status,
      'slug': instance.slug,
      'sorting': instance.sorting,
      'views': instance.views,
      'price': instance.price,
      'currency_id': instance.currencyId,
      'presale_price_text': instance.presalePriceText,
      'rating': instance.rating,
      'updated_at': instance.updatedAt,
      'images_bag': instance.imagesBag,
      'public_link': instance.publicLink,
      'description': instance.description,
      'short_description': instance.shortDescription,
      'user': instance.user,
      'points_price': instance.pointsPrice,
      'related_items': instance.relatedItems,
      'options_data': instance.optionsData,
      'reviews_list': instance.reviewsList,
      'fields_data': instance.fieldsData,
      'video_url': instance.videoUrl,
      'images_data': instance.imagesData,
      'category_text': instance.categoryText,
      'comments': instance.comments,
      'featured': instance.featured,
    };
