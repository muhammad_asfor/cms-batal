// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'slide.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Slide _$SlideFromJson(Map<String, dynamic> json) => Slide(
      id: json['id'] as int,
      createdAt: json['created_at'] as String,
      status: json['status'] as int,
      entity: json['entity'] as String,
      coverImage: json['cover_image'] as String?,
      entityId: json['entity_id'] as int?,
      images:
          (json['images'] as List<dynamic>?)?.map((e) => e as String).toList(),
      link: json['link'] as String?,
      slug: json['slug'] as String?,
      sorting: json['sorting'] as int?,
      title: json['title'] as String?,
    );

Map<String, dynamic> _$SlideToJson(Slide instance) => <String, dynamic>{
      'id': instance.id,
      'entity': instance.entity,
      'entity_id': instance.entityId,
      'link': instance.link,
      'images': instance.images,
      'status': instance.status,
      'slug': instance.slug,
      'sorting': instance.sorting,
      'created_at': instance.createdAt,
      'cover_image': instance.coverImage,
      'title': instance.title,
    };
