import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/field_data.dart';

part 'comment.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Comment {
  final int id;
  final User? user;
  final String? comment;

  Comment({
    required this.id,
    this.user,
    this.comment,
  });

  factory Comment.fromJson(json) => _$CommentFromJson(json);

  toJson() => _$CommentToJson(this);

  static List<Comment> fromJsonList(List json) {
    return json.map((e) => Comment.fromJson(e)).toList();
  }
}
