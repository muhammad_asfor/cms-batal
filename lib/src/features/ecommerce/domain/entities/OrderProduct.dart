import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/SharedEntities.dart';
part 'OrderProduct.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class OrderProduct {
  final int orderItemId;
  final String? title;
  final String coverImage;
  final bool? canReview;
  final String description;
  final String priceText;
  final int? itemId;
  final List<OptionData>? optionsData;
  OrderProduct(this.orderItemId, this.description, this.priceText, this.title,
      this.coverImage, this.canReview,{this.itemId,this.optionsData});

  factory OrderProduct.fromJson(json) => _$OrderProductFromJson(json);
  toJson() => _$OrderProductToJson(this);

  static List<OrderProduct> fromJsonList(List json) {
    return json.map((e) => OrderProduct.fromJson(e)).toList();
  }
}
