import 'package:dio/dio.dart';
import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:equatable/equatable.dart';
import 'package:dartz/dartz.dart';

class Login extends UseCase<User, LoginParams> {
  final AuthRepository repository;

  Login(
    this.repository,
  );
  @override
  Future<Either<Failure, User>> call(LoginParams params) async {
    return await repository.login(
      email: params.email.trim(),
      password: params.password,
      tokenDio:params.tokenDio
    );
  }
}

class LoginParams extends Equatable {
  final String email;
  final String password;
  final Dio? tokenDio;
  LoginParams({required this.email, required this.password, this.tokenDio});

  @override
  List<Object?> get props => [email, password];
}
