import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/Address.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetMyAddresses extends UseCase<List<Address>, NoParams> {
  final EcommerceRepository repository;
  GetMyAddresses(this.repository);

  @override
  Future<Either<Failure, List<Address>>> call(NoParams noParams) async {
    return await repository.getMyAddresses();
  }
}
