import 'package:json_annotation/json_annotation.dart';

part 'blog.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Blog {
  final int id;
  final String title;
  final String coverImage;
  final String description;
  final List<String>? imagesBag;

  Blog({
    required this.title,
    required this.description,
    required this.coverImage,
    required this.id,
    this.imagesBag,
  });

  factory Blog.fromJson(json) => _$BlogFromJson(json);
  toJson() => _$BlogToJson(this);

  static List<Blog> fromJsonList(List json) {
    return json.map((e) => Blog.fromJson(e)).toList();
  }
}
