// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OrderProduct.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderProduct _$OrderProductFromJson(Map<String, dynamic> json) => OrderProduct(
      json['order_item_id'] as int,
      json['description'] as String,
      json['price_text'] as String,
      json['title'] as String?,
      json['cover_image'] as String,
      json['can_review'] as bool?,
      itemId: json['item_id'] as int?,
      optionsData: (json['options_data'] as List<dynamic>?)
          ?.map((e) => OptionData.fromJson(e))
          .toList(),
    );

Map<String, dynamic> _$OrderProductToJson(OrderProduct instance) =>
    <String, dynamic>{
      'order_item_id': instance.orderItemId,
      'title': instance.title,
      'cover_image': instance.coverImage,
      'can_review': instance.canReview,
      'description': instance.description,
      'price_text': instance.priceText,
      'item_id': instance.itemId,
      'options_data': instance.optionsData,
    };
