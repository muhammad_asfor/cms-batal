import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../settings.dart';

class AuthSharedPrefs {
  final SharedPreferences _preferences;
  AuthSharedPrefs(this._preferences);

  saveAuthUser(User user) async {
    print('old token is ${_preferences.getString(
        AuthConstants.User_Token)}');
    await _preferences.setString(AuthConstants.EMAIL, user.email ?? "");
    await _preferences.setString(AuthConstants.ID, user.id.toString());
    await _preferences.setString(
        AuthConstants.User_Token, user.oauth?.accessToken ?? "");
    await _preferences.setString(
        AuthConstants.refreashToken, user.oauth?.refreshToken ?? "");
    await _preferences.setString(AuthConstants.NAME, user.name ?? "");
    await _preferences.setString(AuthConstants.ADDRESS, user.address ?? "");
    await _preferences.setString(AuthConstants.MOBILE, user.mobile ?? "");
    await _preferences.setString(AuthConstants.WEBSITE, user.website ?? "");
    await _preferences.setString(AuthConstants.PASSWORD, user.password ?? "");
    print('new token is ${_preferences.getString(
        AuthConstants.User_Token)}');
    return;
  }

  saveUserToken(String token, String refreashToken) {
    _preferences.setString(AuthConstants.User_Token, token);
    _preferences.setString(AuthConstants.refreashToken, refreashToken);
  }

  saveRefreashToken(String token) {
    _preferences.setString(AuthConstants.refreashToken, token);
  }

  Future<String> getRefreashToken() async {
    return _preferences.getString(AuthConstants.refreashToken) ?? "";
  }

  Future<bool> getIsLogin() async {
    return _preferences.getBool(AuthConstants.ISLOGGEDIN) ?? false;
  }

  Future<bool> logout() async {
    _preferences.clear();
    return _preferences.setBool(AuthConstants.ISLOGGEDIN, false);
  }

  Future<String?> getUserToken() async {
    return _preferences.getString(AuthConstants.User_Token);
  }

  Future<String?> getPublicToken() async {
    return _preferences.getString(AuthConstants.Public_Token);
  }

  Future<void> setPublicToken(String token) async {
    await _preferences.setString(AuthConstants.Public_Token, token);

    return;
  }

  Future<void> setIsLogin(bool bySocial) async {
    await _preferences.setBool(AuthConstants.ISLOGGEDIN, bySocial);
    await _preferences.setBool(AuthConstants.ISLOGGEDIN, true);
    return;
  }

  Future<bool> getIsBySocial() async {
    return _preferences.getBool(AuthConstants.ISLOGGEDIN) ?? false;
  }

  Future<User> getEmailPassword() async {
    final id = _preferences.getString(AuthConstants.ID);
    final email = _preferences.getString(AuthConstants.EMAIL);
    final password = _preferences.getString(AuthConstants.PASSWORD);
    return User(email: email, id: int.parse(id!), password: password);
  }
}
