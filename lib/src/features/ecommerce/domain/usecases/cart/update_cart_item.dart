import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class UpdateCartItem extends UseCase<bool, UpdateCartItemParams> {
  final EcommerceRepository repository;
  UpdateCartItem(this.repository);

  @override
  Future<Either<Failure, bool>> call(UpdateCartItemParams params) async {
    final result = await repository.updateCartItem(
      cartItemId: params.cartItemId,
      qty: params.qty,
    );
   

    return result;
  }
}

class UpdateCartItemParams {
  final int cartItemId;
  final int qty;

  UpdateCartItemParams({
    required this.cartItemId,
    required this.qty,
  });
}
