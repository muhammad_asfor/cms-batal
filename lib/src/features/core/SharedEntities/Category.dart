import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/dynamic_field.dart';
part 'Category.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Category {
  final int id;
  final int? parentId;
  final int? featured;
  final String coverImage;
  final String title;
  final String? description;
  final List<Category>? subCategories;
  List<DynamicField>? dynamicFilterFields;
  // Null parent;

  Category({
    required this.id,
     this.subCategories,
    this.parentId,
    this.featured,
    required this.coverImage,
    required this.title,
    this.description,
    this.dynamicFilterFields,
  });

  factory Category.fromJson(json) => _$CategoryFromJson(json);
  toJson() => _$CategoryToJson(this);
  static List<Category> fromJsonList(List json) {
    return json.map((e) => Category.fromJson(e)).toList();
  }
}
