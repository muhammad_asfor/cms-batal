import 'package:progiom_cms/src/features/ecommerce/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EcommerceSharedPrefs {
  final SharedPreferences _preferences;
  EcommerceSharedPrefs(this._preferences);

  saveLocalQuery(String query) async {
    final result = _preferences.getStringList(localsearches);
    if (result != null) {
      if (!result.contains(query)) {
        result.add(query);
        if (result.length > 5) result.removeAt(0);
        _preferences.setStringList(localsearches, result);
      }
    } else {
      _preferences.setStringList(localsearches, [query]);
    }
  }

  List<String>? getLocalQueries() {
    final result = _preferences.getStringList(localsearches);
    return result;
  }

  void deleteSearchHistoryItem(String query) {
    final result = _preferences.getStringList(localsearches);
    result?.remove(query);

    _preferences.setStringList(localsearches, result ?? []);
  }
}
