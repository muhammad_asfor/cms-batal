
import 'package:dartz/dartz.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class UpdateNewCurrencyUseCase
    extends UseCase<bool, NoParams> {
  final SettingsRepository repository;

  UpdateNewCurrencyUseCase(
      this.repository,
      );
  @override
  Future<Either<Failure, bool>> call(
      NoParams params) async {
    return await repository.updateNewCurrency();
  }
}