import 'package:json_annotation/json_annotation.dart';

part 'product_review.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ProductReview {
  final int? id;
  final String? name;
  final String? coverImage;
  final int? rate;
  final String? comment;
  final String? createdAt;

  ProductReview(this.id, this.name, this.coverImage, this.rate, this.comment,
      this.createdAt);
  factory ProductReview.fromJson(json) => _$ProductReviewFromJson(json);
  toJson() => _$ProductReviewToJson(this);
}
