// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Service _$ServiceFromJson(Map<String, dynamic> json) => Service(
      id: json['id'] as int,
      parentId: json['parent_id'] as int?,
      featured: json['featured'] as int?,
      categoryText: json['category_text'] as String?,
      coverImage: json['cover_image'] as String,
      title: json['title'] as String,
      user: json['user'] == null ? null : User.fromJson(json['user']),
    );

Map<String, dynamic> _$ServiceToJson(Service instance) => <String, dynamic>{
      'id': instance.id,
      'parent_id': instance.parentId,
      'featured': instance.featured,
      'cover_image': instance.coverImage,
      'title': instance.title,
      'category_text': instance.categoryText,
      'user': instance.user,
    };
