import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/entities/points_registry.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetPointsRegistry extends UseCase<List<PointsRegistry>, NoParams> {
  final EcommerceRepository repository;
  GetPointsRegistry(this.repository);

  @override
  Future<Either<Failure, List<PointsRegistry>>> call(NoParams noParams) async {
    return await repository.getMyPointsRegistry();
  }
}
