// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CountryModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryModel _$CountryModelFromJson(Map<String, dynamic> json) => CountryModel(
      id: json['id'] as int,
      name: json['name'] as String?,
      phonecode: json['phonecode'] as String?,
      currency: json['currency'] as String?,
      emoji: json['emoji'] as String?,
      translations: (json['translations'] as List<dynamic>?)
          ?.map((e) => CountryTranslation.fromJson(e))
          .toList(),
      status: json['status'] as int?,
      title: json['title'] as String?,
    );

Map<String, dynamic> _$CountryModelToJson(CountryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'phonecode': instance.phonecode,
      'currency': instance.currency,
      'emoji': instance.emoji,
      'translations': instance.translations,
      'status': instance.status,
      'title': instance.title,
    };

CountryTranslation _$CountryTranslationFromJson(Map<String, dynamic> json) =>
    CountryTranslation(
      id: json['id'] as int?,
      countryId: json['country_id'] as int?,
      locale: json['locale'] as String?,
      title: json['title'] as String?,
      capital: json['capital'] as String?,
    );

Map<String, dynamic> _$CountryTranslationToJson(CountryTranslation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'country_id': instance.countryId,
      'locale': instance.locale,
      'title': instance.title,
      'capital': instance.capital,
    };
