import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class DeleteSearchHistoryItem
    extends UseCase<bool, DeleteSearchHistoryItemParams> {
  final EcommerceRepository repository;
  DeleteSearchHistoryItem(this.repository);

  @override
  Future<Either<Failure, bool>> call(
      DeleteSearchHistoryItemParams params) async {
    repository.deleteSearchHistoryItem(params.query);
    return Right(true);
  }
}

class DeleteSearchHistoryItemParams {
  final String query;

  DeleteSearchHistoryItemParams({required this.query});
}
