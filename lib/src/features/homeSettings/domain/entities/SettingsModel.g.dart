// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SettingsModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingsModel _$SettingsModelFromJson(Map<String, dynamic> json) =>
    SettingsModel(
      selectedProducts: (json['selected_products'] as List<dynamic>?)
          ?.map((e) => Product.fromJson(e))
          .toList(),
      recentProducts:
          SettingsModel.baseProductFromJson(json['recent_products']),
      filterData: json['filter_data'] == null
          ? null
          : FilterData.fromJson(json['filter_data']),
      featuredProducts:
          SettingsModel.baseProductFromJson(json['featured_products']),
      pages: (json['pages'] as List<dynamic>?)
          ?.map((e) => PageModel.fromJson(e))
          .toList(),
      slides: (json['slides'] as List<dynamic>?)
          ?.map((e) => Slide.fromJson(e))
          .toList(),
      blogPosts: SettingsModel.baseBlogFromJson(json['blog_posts']),
      languages: (json['languages'] as List<dynamic>)
          .map((e) => Languages.fromJson(e))
          .toList(),
      onboards: (json['onboards'] as List<dynamic>)
          .map((e) => Onboards.fromJson(e))
          .toList(),
      categories: (json['categories'] as List<dynamic>?)
          ?.map((e) => Category.fromJson(e))
          .toList(),
      categoriesFeatured: (json['categories_featured'] as List<dynamic>?)
          ?.map((e) => CategoryFeatured.fromJson(e))
          .toList(),
      user: json['user'] == null ? null : User.fromJson(json['user']),
      unreadNotifications: json['unread_notifications'] as int?,
      notices: (json['notices'] as List<dynamic>?)
          ?.map((e) => Notices.fromJson(e))
          .toList(),
      appLatestVersion:
          SettingsModel.versionFromJson(json['app_latest_version']),
      appMinVersion: SettingsModel.versionFromJson(json['app_min_version']),
      currencies: (json['currencies'] as List<dynamic>?)
          ?.map((e) => Currency.fromJson(e))
          .toList(),
      featuredServices: (json['featured_services'] as List<dynamic>?)
          ?.map((e) => Service.fromJson(e))
          .toList(),
    );

Map<String, dynamic> _$SettingsModelToJson(SettingsModel instance) =>
    <String, dynamic>{
      'languages': instance.languages,
      'onboards': instance.onboards,
      'categories': instance.categories,
      'categories_featured': instance.categoriesFeatured,
      'recent_products': instance.recentProducts,
      'featured_products': instance.featuredProducts,
      'selected_products': instance.selectedProducts,
      'user': instance.user,
      'unread_notifications': instance.unreadNotifications,
      'notices': instance.notices,
      'blog_posts': instance.blogPosts,
      'app_latest_version': instance.appLatestVersion,
      'app_min_version': instance.appMinVersion,
      'filter_data': instance.filterData,
      'pages': instance.pages,
      'slides': instance.slides,
      'currencies': instance.currencies,
      'featured_services': instance.featuredServices,
    };
