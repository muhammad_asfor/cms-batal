import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

enum SocialLoginProvider { face, google, apple }

class AuthSettings {
  static late String _clientSecret;
  static late String _clientId;
  static late String _clientPasswordSecret;
  static late String _clientPasswordId;
  static late Function() _navigateLoginCall;
  static String get clientId => _clientId;
  static String get clientSecret => _clientSecret;
  static String get clientPasswordSecret => _clientPasswordSecret;
  static String get clientPasswordId => _clientPasswordId;
  static Function() get navigateLoginCall => _navigateLoginCall;

  static init(String cleintsecret, String clientid, String clientPasswordsecret,
      String clientPasswordid, Function() navigateLoginCall) {
    _clientId = clientid;
    _clientSecret = cleintsecret;
    _clientPasswordId = clientPasswordid;
    _clientPasswordSecret = clientPasswordsecret;
    _navigateLoginCall = navigateLoginCall;
  }
}

class AuthConstants {
  static const String Language = "language";
  static const String Currency = "currency";
  static const String Country = "country";
  static const String EMAIL = "email";
  static const String NAME = "name";
  static const String ID = "id";
  static const String refreashToken = "refreashToken";
  static const String PASSWORD = "password";
  static const String MOBILE = "mobile";
  static const String WEBSITE = "website";
  static const String ADDRESS = "address";
  static const String IMAGE = "image";
  static const String User_Token = "user_token";
  static const String Public_Token = "public_token";

  static const String ISLOGGEDIN = "isLoggedIn";
  static const String BYSOCIAL = "bySocial";
}
